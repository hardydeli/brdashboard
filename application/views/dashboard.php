<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Meeting Room Dashboard | PT. Indojaya Agrinusa</title>
	<!--<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">

</head>
<body>

<div class="container p-0">
	<div id="incoming-section">
		<header>
			<span id='logo'><img src="<?php echo base_url(); ?>img/logo-japfa.png" alt="Logo Japfa" width='15%'></span>
			<h1 id="title">Meeting Room</h1>
		</header>
		<div id="incomingMeetingCarousel" class="carousel slide" data-bs-ride="carousel">
			<div class="carousel-inner mt-5" id="list-incoming-meeting">
				<!--<div class="carousel-item active">
					<img src="" class="d-block w-100" alt="">
					<h2>Benefeed</h2>
					<p class="m-0">Acara Meeting Pak Eko</p>
					<p>27 Januari 2023</p>
					<p>08:00 - 12:00 AM</p>
				</div>
				<div class="carousel-item">	
					<img src="" class="d-block w-100" alt="">
					<h2>Comfeed</h2>
					<p class="m-0">28 Januari 2023</p>
					<p>Acara Meeting Pak Wawan</p>
					<p>08:00 - 12:00 AM</p>
				</div>
				<div class="carousel-item">
					<img src="" class="d-block w-100" alt="">
					<h2>STP</h2>
					<p class="m-0">28 Januari 2023</p>
					<p>Acara Meeting Pak Gilbert</p>
					<p>08:00 - 12:00 AM</p>
				</div>-->
			</div>
		</div>
	</div>
	<div id="today-section">
		<input type="hidden" id="datetime" name="datetime" value="<?php echo $datetime ?>">
		<h2>Today</h2>
		<div id="time_display" class="mb-2"></div>
		<div class="list-card" id="list-today-meeting">
			<!--<div class="card">
				<div class="card-body">
					<h4 class="card-title m-0">Meeting Dengan Kementrian</h5>
					<p class="card-text m-0">Comfeed</p>
					<p class="card-text m-0">08:00 - 12:00 AM</p>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title m-0">Meeting Dengan Kementrian</h5>
					<p class="card-text m-0">Comfeed</p>
					<p class="card-text m-0">08:00 - 12:00 AM</p>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title m-0">Meeting Dengan Kementrian</h5>
					<p class="card-text m-0">Comfeed</p>
					<p class="card-text m-0">08:00 - 12:00 AM</p>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title m-0">Meeting Dengan Kementrian</h5>
					<p class="card-text m-0">Comfeed</p>
					<p class="card-text m-0">08:00 - 12:00 AM</p>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title m-0">Meeting Dengan Kementrian</h5>
					<p class="card-text m-0">Comfeed</p>
					<p class="card-text m-0">08:00 - 12:00 AM</p>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title m-0">Meeting Dengan Kementrian</h5>
					<p class="card-text m-0">Comfeed</p>
					<p class="card-text m-0">08:00 - 12:00 AM</p>
				</div>
			</div>-->
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>js/jquery-3.6.3.min.js" crossorigin="anonymous"></script>
<script>
	function addSeconds(date, seconds) {
		date.setSeconds(date.getSeconds() + seconds);
		return date;
	}
	function display_ct6() {
		//var x = new Date()
		var serverdate = new Date($('#datetime').val());
		var x = addSeconds(serverdate,1);
		$('#datetime').val(x);

		var ampm = x.getHours( ) >= 12 ? ' PM' : ' AM';
		hours = x.getHours( ) % 12;
		hours = hours ? hours : 12;
		//var x1=x.getMonth() + 1+ "/" + x.getDate() + "/" + x.getFullYear(); 
		var x1 = hours + ":" +  x.getMinutes().toString().padStart(2,'0') + ":" +  x.getSeconds().toString().padStart(2,'0')  + ampm;
		document.getElementById('time_display').innerHTML = x1;
    	display_c6();
	}
	function display_c6(){
		var refresh=1000; // Refresh rate in milli seconds
		mytime=setTimeout('display_ct6()',refresh);
	}
	function display_today_meeting(){
		$.ajax({
          type : 'ajax',
          url : '<?php echo site_url('welcome/getTodayMeeting') ?>',		
		  async : true,
          datatype : 'json',
          success: function(data){
			var html = '';
			var i;
			var dt = JSON.parse(data);
			for(var x of dt){
				startdate = new Date(x.start_meeting);
				enddate = new Date(x.end_meeting);
				var ampm1 = startdate.getHours( ) >= 12 ? ' PM' : ' AM';
				hours1 = startdate.getHours( ) % 12;
				hours1 = hours1 ? hours1 : 12;
				var x1 = hours1 + ":" +  startdate.getMinutes().toString().padStart(2,'0') + " " + ampm1;

				var ampm2 = enddate.getHours( ) >= 12 ? ' PM' : ' AM';
				hours2 = enddate.getHours( ) % 12;
				hours2 = hours2 ? hours2 : 12;
				var x2 = hours2 + ":" +  enddate.getMinutes().toString().padStart(2,'0') + " " + ampm2;

				html += '<div class="card">' +
							'<div class="card-body">' +
								'<h4 class="card-title m-0">'+x.header_meet+'</h4>' +
								'<p class="card-text m-0">'+x.room_name+'</p>' +
								'<p class="card-text m-0">'+x1+' - '+x2+'</p>' +
							'</div>' +
						'</div>';
			}
			if (dt.length<=0) {
                                html = '<div><style scoped>#no-today-meeting {color:azure;font-size:3em}</style><p id="no-today-meeting">No Today Meeting</p></div>';
                        }
			$('#list-today-meeting').html(html);
          }
        });
		setTimeout(display_today_meeting, 300000);
	}
	function display_incoming_meeting(){
		$.ajax({
          type : 'ajax',
          url : '<?php echo site_url('welcome/getIncomingMeeting') ?>',		
		  async : true,
          datatype : 'json',
          success: function(data){
			var html = '';
			var i;
			var dt = JSON.parse(data);
			for(i=0;i<dt.length;i++){
				startdate = new Date(dt[i].start_meeting);
				enddate = new Date(dt[i].end_meeting);
				var ampm1 = startdate.getHours( ) >= 12 ? ' PM' : ' AM';
				hours1 = startdate.getHours( ) % 12;
				hours1 = hours1 ? hours1 : 12;
				var x1 = hours1 + ":" +  startdate.getMinutes().toString().padStart(2,'0') + " " + ampm1;

				var ampm2 = enddate.getHours( ) >= 12 ? ' PM' : ' AM';
				hours2 = enddate.getHours( ) % 12;
				hours2 = hours2 ? hours2 : 12;
				var x2 = hours2 + ":" +  enddate.getMinutes().toString().padStart(2,'0') + " " + ampm2;

				var meetingdate = startdate.getDate() + ' ' + startdate.toLocaleString('default', { month: 'long' }) + ' ' + startdate.getFullYear();
				html += '<div class="carousel-item ' + (i == 0 ? 'active' : '') +'">'+
						'<img src="" class="d-block w-100" alt="">'+
						'<h2>'+dt[i].room_name+'</h2>'+
						'<p class="m-0">'+ dt[i].header_meet + '</p>'+
						'<p>'+meetingdate+'</p>'+
						'<p>'+x1+' - '+x2+'</p>'+
					'</div>';
			}
			if (dt.length<=0) {
				html = '<div><style scoped>#no-incoming-meeting {font-size:3em}</style><p id="no-incoming-meeting">No Incoming Meeting</p></div>';
			}
			$('#list-incoming-meeting').html(html);
          }
        });
		setTimeout(display_incoming_meeting, 300000);
	}
	display_ct6();
	$(document).ready(function(){
		display_today_meeting();
		display_incoming_meeting();
	});
</script>
</body>
</html>

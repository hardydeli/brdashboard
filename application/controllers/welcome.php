<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->model('dashboard_model');
		$data['datetime']=$this->dashboard_model->getDate()->waktu;
		$this->load->view('dashboard',$data);
	}
	public function getIncomingMeeting(){
		$this->load->model('dashboard_model');
		$data = $this->dashboard_model->getByTable('vmrbs_entry_biggerdate');
		echo json_encode($data);
	}
	public function getTodayMeeting(){
		$this->load->model('dashboard_model');
		$data = $this->dashboard_model->getByTable('vmrbs_entry_today');
		echo json_encode($data);
	}
}

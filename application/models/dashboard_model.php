<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard_model extends CI_model{
    function getDate(){
        return $this->db->query('select sysdate() as waktu')->row();
    }
    function getByTable($table){
        return $this->db->get($table)->result();
    }
}
?>
